const getSum = (str1, str2) => {
  // add your implementation below
  let newSum = '';
  let sum = parseInt(str1) + parseInt(str2);
  if (isNaN(sum)) {
    return false;
  }
  if (
    typeof str1 === 'object' ||
    typeof str2 === 'object' ||
    typeof str1 === 'number' ||
    typeof str2 === 'number'
  ) {
    return false;
  } else {
    sum = parseInt(str1) + parseInt(str2);
  }
  newSum += sum;
  return newSum;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let numOfPosts = 0;
  let numOfComment = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      numOfPosts = numOfPosts + 1;
    }
  }
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].comments) {
      for (let j = 0; j < listOfPosts[i].comments.length; j++) {
        if (listOfPosts[i].comments[j].author === authorName) {
          numOfComment = numOfComment + 1;
        }
      }
    } else;
  }
  return `Post:${numOfPosts},comments:${numOfComment}`;
};

const tickets = (people) => {
  // add your implementation below
  let sum = parseInt(people[0]);
  if (sum > 25) {
    return 'NO';
  }
  for (let i = 1; i < people.length; i++) {
    if (people[i] - sum > 25) {
      return 'NO';
    }
    sum += 25;
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
